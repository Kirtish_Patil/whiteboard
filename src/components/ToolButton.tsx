import { RefObject } from "react"

interface ToolButtonProps {
  onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void
  toolName?: string
  ref?: RefObject<HTMLButtonElement>
}

const ToolButton: React.FC<ToolButtonProps> = ({ onClick, toolName }) => {
  return (
    <button
      className={
        "hover:bg-violet-600 active:bg-violet-700 focus:outline-none focus:ring focus:ring-violet-500 focus:bg-violet-400  hover:text-white p-2 border-solid border-gray-500 border-2 bg-purple-100"
      }
      onClick={onClick}
    >
      {toolName}
    </button>
  )
}

export default ToolButton
