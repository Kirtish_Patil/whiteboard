import React, { useEffect, useRef, useState, memo } from "react"
import { MouseEvent } from "react"
import Tool from "./ToolButton"
import ToolButton from "./ToolButton"
import Switcher13 from "./Switcher13"

interface CanvasProps {
  width?: number
  height?: number
  drawFunction: (context: CanvasRenderingContext2D) => void
  e?: MouseEvent
}

const Canvas: React.FC = () => {
  const inputRef = useRef<HTMLInputElement>(null)
  const input = inputRef.current

  const tools = {
    pen: "pen",
    eraser: "eraser",
    clearScreen: "clearScreen",
  }
  const [isDotted, setIsDotted] = useState(false)
  const [toolType, setToolType] = useState(tools.pen)
  const [isDrawing, setIsDrawing] = useState(false)
  const canvasRef = useRef<HTMLCanvasElement>(null)
  const contextRef = useRef<CanvasRenderingContext2D | null>(null)
  const c = contextRef.current

  // const [lineWidth, setLineWidth] = useState<number>(10)
  const bgColor = "#ffffff"

  useEffect(() => {
    const canvas = canvasRef.current
    if (!canvas) return
    const c = canvas.getContext("2d")
    if (!c) return
    contextRef.current = c

    canvas.width = window.innerWidth * 2
    canvas.height = window.innerHeight * 2
    canvas.style.width = `${window.innerWidth}px`
    canvas.style.height = `${window.innerHeight}px`

    c.scale(2, 2)
    c.fillStyle = bgColor
    c.fillRect(0, 0, window.innerWidth, window.innerHeight)

    c.setLineDash([])
    c.lineCap = "round"
    c.lineJoin = "round"
    c.lineWidth = 10
    c.strokeStyle = "black"
  }, [])

  const startDrawing = (e: MouseEvent) => {
    console.log("startDrawing : " + e)
    setIsDrawing(true)
    const canvas = canvasRef.current
    if (!canvas) {
      return
    }

    const c = canvas.getContext("2d")
    if (!c) {
      return
    }
    c?.beginPath()
    c?.moveTo(e.nativeEvent.offsetX, e.nativeEvent.offsetY)
    c.closePath()
  }

  const stopDrawing = (e: MouseEvent) => {
    console.log("stopDrawing : " + e)
    setIsDrawing(false)
  }

  const drawing = (e: MouseEvent) => {
    const canvas = canvasRef.current
    if (!canvas) {
      return
    }

    const c = canvas.getContext("2d")
    if (!c) {
      return
    }
    if (!c) return
    if (!isDrawing) return
    console.log("drawing : " + e)
    c.lineTo(e.nativeEvent.offsetX, e.nativeEvent.offsetY)
    c.stroke()
  }

  const changeTool = (toolType: string) => {
    setToolType(toolType)
    const canvas = canvasRef.current
    if (!canvas) {
      return
    }

    const c = canvas.getContext("2d")
    if (!c) {
      return
    }

    if (toolType === "eraser") {
      c.strokeStyle = "white"
      c.setLineDash([])
    } else if (toolType === "pen") {
      console.log("pen selected")
      c.strokeStyle = "black"
    } else if (toolType === "clearScreen") {
      c.fillStyle = "white"
      c.fillRect(0, 0, canvas.width, canvas.height)
      setToolType(tools.pen)
    }
  }

  const changeStyle = () => {
    if (!input) {
      return
    }
    if (input.checked === true) {
      setIsDotted(true)
      c?.setLineDash([10, 20])
      console.log(isDotted)
    } else if (input.checked === false) {
      setIsDotted(false)
      c?.setLineDash([])
      console.log(isDotted)
    }
  }

  // const changeLineWidth = (e: React.ChangeEvent<HTMLInputElement>) => {
  //   const value = Number(e.target.value)
  //   setLineWidth(value)
  //   console.log(lineWidth)
  // }

  const changeLineWidth = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (!c) {
      return
    }
    const value = Number(e.target.value)
    c.lineWidth = value
    if (isDotted) {
      c.setLineDash([value, value * 2])
    }
  }

  return (
    <>
      <br></br>
      {(toolType === "pen" || toolType === "eraser") && (
        <div className="bg-slate-200 justify-around flex-col absolute">
          <div className="  flex justify-start gap-5">
            <h3>Dotted</h3>
            <input
              ref={inputRef}
              type="checkbox"
              onChange={() => changeStyle()}
            />
          </div>
          <div>
            <h3>line thickness</h3>
            <input
              type="range"
              min={10}
              max={200}
              onChange={(e) => changeLineWidth(e)}
            />
          </div>
        </div>
      )}

      <div className="absolute bg-slate-100 flex flex-row mx-auto w-auto items-center justify-items-center bottom-0 mb-4 translate-x-[-50%] left-1/2">
        <Tool
          onClick={(e) => {
            e.preventDefault()
            changeTool(tools.pen)
          }}
          toolName={"Pen"}
        />
        <Tool onClick={() => changeTool(tools.eraser)} toolName={"Eraser"} />
        <Tool
          onClick={() => changeTool(tools.clearScreen)}
          toolName={"Ahmed Solanki"}
        />
      </div>
      <canvas
        ref={canvasRef}
        onMouseDown={(e) => startDrawing(e)}
        onMouseMove={(e) => drawing(e)}
        onMouseUp={(e) => stopDrawing(e)}
      />
    </>
  )
}

export default Canvas

// Bezier Curve Logic

// let initialx = 200
// let initialy = 200
// let endx = 700
// let endy = 200
// let midx = 200
// let midy = 800

// c.strokeStyle = "white"
// c.moveTo(initialx, initialy)
// c.quadraticCurveTo(midx, midy, endx, endy)
// c.lineWidth = 10
// c.stroke()
