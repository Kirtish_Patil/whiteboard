import React, { useEffect } from "react"
import { useRef } from "react"

const Canvas2 = () => {
  const canvasRef = useRef<HTMLCanvasElement>(null)
  const contextRef = useRef<CanvasRenderingContext2D | null>(null)
  const c = contextRef.current
  console.log(c)

  useEffect(() => {
    const canvas = canvasRef.current
    if (!canvas) return
    const c = canvas.getContext("2d")
    if (!c) return
    contextRef.current = c

    c.fillStyle = "pink"
    c.fillRect(0, 0, canvas.width, canvas.height)

    c.strokeStyle = "orange"
    // c.beginPath()
    c.moveTo(20, 20)
    c.quadraticCurveTo(20, 100, 200, 20)
    c.stroke()
  }, [])
  return <canvas ref={canvasRef} />
}

export default Canvas2
