pipeline {
    agent any

    environment {
        NEXUS_REPO_URL = 'http://206.189.129.73:5000/repository/docker-hosted/'
        DOCKER_IMAGE_VERSION = "1.0.0"
        DOCKER_IMAGE_TAG = "whiteboard"
        DOCKER_IMAGE_TYPE = "feature"
        STAGING_SERVER_IP = ""
        PROD_SERVER_IP = ""
        SSH_PRIVATE_KEY_PATH="/var/jenkins_home/.ssh/id_rsa"
    }

    stages {
        stage("Checkout from SCM"){
            steps {
                sh 'echo "SCM checkout of branch \'$GIT_BRANCH\'"'
                git branch: env.GIT_BRANCH, credentialsId: 'gitlab-cred', url: 'https://gitlab.com/Kirtish_Patil/whiteboard.git'
            }
        }

        stage("Testing the application"){
            steps {
                script {
                    sh '''
                    echo "Installing npm packages"
                    npm i
                    echo "Running tests"
                    npm run test
                    if [ $? -ne 0 ]; then
                        echo "Tests Failed"
                        exit 1
                    fi
                    echo "Tests Completed Successfully"
                    '''
                }
            }
        }

        // MASTER 
        stage("Building Docker Image") { 
            // when {
            //     branch 'master'
            // }
            steps {
                sh 'echo "Building Docker Image for branch \'$GIT_BRANCH\'" of tag \'${DOCKER_IMAGE_TAG}-${DOCKER_IMAGE_TYPE}:${DOCKER_IMAGE_VERSION}\''
                script {
                    dockerImage = docker.build("${DOCKER_IMAGE_TAG}-${DOCKER_IMAGE_TYPE}:${DOCKER_IMAGE_VERSION}")
                }
            }
        }

        stage("Push Docker image to Nexus"){
            steps {
                script {
                    docker.withRegistry("${NEXUS_REPO_URL}", 'nexus-repo-cred'){
                        dockerImage.push()
                    }
                }
            }
        }

        stage("Deploying to staging environment"){
            when {
                branch 'master'
            }
            steps {
                sh '''
                echo "Deploying to master"
                ansible-playbook -i ansible/hosts  ansible/setup-docker.yml | tee ansible_setup_output.txt
                ansible-playbook -i ansible/hosts  ansible/deploy-app.yml | tee ansible_deploy_output.txt

                echo "===== Ansible Setup Output ====="
                cat ansible_setup_output.txt
                echo "===== Ansible Deploy Output ====="
                cat ansible_deploy_output.txt
                '''
            }
        }

        stage("Global stage for all branches") {
            steps {
                echo "Process was successful..."
            }
        }
    }
    post {
        always {
            script {
                cleanWs()
                sh 'docker image prune -f'
            }
        }
    }
}


// sh '''
// docker login --username nx-docker --password 123 http://206.189.129.73:5000/repository/docker-hosted/
// '''
